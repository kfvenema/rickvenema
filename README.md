# Rick Venema
My strong points:

* Machine Learning
* Statistics
* Programming 

In school, I liked the programming assignments and loved statistics. But most of all, I loved the machine learning/data mining part. It baffled me that you could use such a wonderful principle and apply it to huge datasets, and (after training models) identify new instances really quickly. This came forward in my project about machine learning in forest type classification. The programming and statistics that I liked came to practice in the modeling of Soil Organic Compound storage in forests and agricultural fields. The model was already coded, but we have written an article about improving that model to be more versatile and how it could be expanded.

## References 
### Machine learning
[Forest Classification Java Wrapper](https://bitbucket.org/kfvenema/forest_type_mlp_model/src/master/)
[Forest Classfication Article Repository](https://github.com/RickVenema/Thema9)

### Modeling
[Improving on SOC models](https://github.com/RickVenema/Thema8)

### Portfolio
[Portfolio website](https://rickvenema.github.io/)